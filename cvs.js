var mousePressed = false;
var lastX, lastY;
var canvas, ctx;
var penColor;
var memory=[];
var n = -1;

function Init(){
    penColor="-1";
    canvas = document.getElementById("mycanvas");
    
    ctx = canvas.getContext("2d");
    ctx.strokeStyle = $("#hex input").val();
    $("#mycanvas").mousedown(function (e) {
        mousePressed = true;
        draw(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, false);
    });
    $("#mycanvas").mousemove(function (e) {
        if (mousePressed) {
            draw(e.pageX - this.offsetLeft, e.pageY - this.offsetTop, true);
        }
    });
    $("#mycanvas").mouseup(function (e) {
        mousePressed = false;
    });
	$("#mycanvas").mouseleave(function (e) {
        mousePressed = false;
    });

    


    var color_picker = document.getElementById("colorPicker").getContext('2d');
    var pick_img = new Image();
    pick_img.src = "pick.png";
    $(pick_img).load(function(){
        color_picker.drawImage(pick_img,0,0);
    });
    $("#colorPicker").click(function(event){
        var a = event.pageX - this.offsetLeft;
        var b = event.pageY - this.offsetTop;
        var img_data = color_picker.getImageData(a, b, 1, 1).data;
        var R = img_data[0];
        var G = img_data[1];
        var B = img_data[2];
        var hex = rgbToHex(R,G,B);
        $("#hex").val('#' + hex);
      });
}
function rgbToHex(R,G,B) {return toHex(R)+toHex(G)+toHex(B)}

function toHex(n) {
    n = parseInt(n,10);
    if (isNaN(n)) return "00";
    n = Math.max(0,Math.min(n,255));
    return "0123456789ABCDEF".charAt((n-n%16)/16)  + "0123456789ABCDEF".charAt(n%16);
}



function draw(x, y, isDown) {
    if (isDown) {
        ctx.beginPath();
        ctx.strokeStyle = $("#hex").val();
        ctx.lineWidth = $("#sizebox").val();
        ctx.lineJoin = "round";
        ctx.moveTo(lastX, lastY);
        ctx.lineTo(x, y);
        ctx.closePath();
        ctx.stroke();
    }
    lastX = x; lastY = y;
}








function pencil() {
    if(penColor=="-1") penColor = "#000000"
    $("#hex").val(penColor);
}

function eraser() {
    var tmp = document.getElementById("hex").value;
    penColor = tmp;
    $("#hex").val("#ffffff");

}

function drawCircle() {
    

}

function drawRec() {


    ctx.fillStyle = "green";
    ctx.fillRect(10, 10, 100, 100);

}

function drawLine(){

    
}

function inputText(){


}


function undo(){

}

function redo() {

}

function deleteImg(){
    ctx.setTransform(1, 0, 0, 1, 0, 0);
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}
document.getElementById("btn_dl").addEventListener('click', function(){
    this.href = document.getElementById("mycanvas").toDataURL();
}, false);